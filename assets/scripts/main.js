import MobileMenu from "./modules/mobileMenu";
import revealOnScroll from "./modules/revealOnScroll";
import StickyHeader from "./modules/stickyHeader";
import Modal from "./modules/modal";
import $ from "jquery";

var mobileMenu = new MobileMenu();
var stickyHeader = new StickyHeader();
var modal = new Modal();

new revealOnScroll( $(".feature-item"), "85%");
new revealOnScroll( $(".testimonial" ), "75%");